import { Component, Input } from '@angular/core';

@Component({
	selector: 'mdi',
	template: `<i class="material-icons" [style.font-size]="(size * 2) + 'px'" [innerHtml]="icon"></i>`,
	host: {
		'[style.height.px]': 'size * 2',
		'[style.width.px]': 'size * 2'
	}
})
export class MaterialDesignIconComponent {
	@Input() icon: string;
	@Input() size = 24;
}
