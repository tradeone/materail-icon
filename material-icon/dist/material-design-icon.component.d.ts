export declare class MaterialDesignIconComponent {
    icon: string;
    size: number;
}
